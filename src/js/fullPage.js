// fullPage.js init

(function () {
	const fullPageSelector = $('#slider');

	fullPageSelector.fullpage({

		// Navigation
		menu: '#nav',
		anchors: ['header', 'about', 'products', 'tariffs', 'contact', 'footer'],

		// Scrolling
		scrollingSpeed: 1500,
		easingcss3: 'cubic-bezier(0.77, 0, 0.175, 1)',

		// Design
		controlArrows: false,
		fixedElements: '#nav, #sliderStatus, #popUp, #popUpDarken',

		// Accessibility
		animateAnchor: true,
		recordHistory: false,

		// Custom selectors
		sectionSelector: '.slider_slide',

		// Actions
		onLeave: function(index, nextIndex, direction) {
			var leavingSection = $(this);
			const sliderStatus = $('#sliderStatus') ;
			const sliderStatusLine = $('#sliderStatusLine');

			// Change slider status line height on scrolling
			sliderStatusLine.height(`${nextIndex * 16.66}%`);

			// Rotate slider status square on scrolling
			$('.slider_status_square').removeClass('is-active');
			$('.slider_status_square:eq(' + (nextIndex - 1) + ')').addClass('is-active');

			// Change color theme for slider status on scrolling
			if (nextIndex == 1 || nextIndex == 4) {
				sliderStatus.removeClass('is-dark');
				sliderStatus.addClass('is-light');
			} else {
				sliderStatus.removeClass('is-light');
				sliderStatus.addClass('is-dark');
			};

			// Closing nav on scrolling
			if ($('#nav').hasClass('is-open')) { $('#nav').removeClass('is-open') };

			// Add class for animation contact section
			if (nextIndex == 5 || nextIndex == 6) {
				$('.slider_slide.contact').addClass('active-last');
			} else {
				$('.slider_slide.contact').removeClass('active-last');
			}

		},
	});

	$(window).resize(function() {
		$.fn.fullpage.reBuild();
			
		setTimeout(function () {
			$.fn.fullpage.fitToSection();
		}, 250);
	});

	$('#feedbackForm input[type="submit"]').click(function() {
		setTimeout(function () {
			$.fn.fullpage.fitToSection();
		}, 250);
	});

}());