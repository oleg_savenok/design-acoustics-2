(function() {
    
    const buttons = $(".products_product_arrows_item");
    let imagesActiveIndex;
    let imagesLenght;

    buttons.click(function() {
        let images = $(".products_product_images_item.slick-active");
        let allNavItems = $(".products_product_nav .slick-slide.slick-active li");
        imagesActiveIndex = $(".products_product_images_item.slick-active img.is-active").index();
        imagesLenght = $(".products_product_images_item.slick-active img").length;

        if ($(this).hasClass("prev")) {
            $(images[0].children[imagesActiveIndex]).removeClass("is-active");
            $(allNavItems).removeClass("is-active");
            
            if (imagesActiveIndex > 0) {
                $(images[0].children[imagesActiveIndex - 1]).addClass("is-active");
                $(allNavItems[imagesActiveIndex - 1]).addClass("is-active");
            } else {
                $(images[0].children[imagesLenght - 1]).addClass("is-active");
                $(allNavItems[imagesLenght - 1]).addClass("is-active");
            }
        } else 
        if ($(this).hasClass("next")) {
            $(images[0].children[imagesActiveIndex]).removeClass("is-active");
            $(allNavItems).removeClass("is-active");

            if (imagesActiveIndex < (imagesLenght - 1)) {
                $(images[0].children[imagesActiveIndex + 1]).addClass("is-active");
                $(allNavItems[imagesActiveIndex + 1]).addClass("is-active");
            } else {
                $(images[0].children[0]).addClass("is-active");
                $(allNavItems[0]).addClass("is-active");
            }
        }
    });

}());