// Open more paragraph text on about section after click on button

(function () {

	const button = $('#aboutOpenText');
	const text = $('#aboutText');

	button.click(function (e) {
		text.toggleClass('is-open');

		if (text.hasClass('is-open')) {
			e.target.innerText = "Менше";
		} else {
			e.target.innerText = "Детальніше";
		}
	});

}());