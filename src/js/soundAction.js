// Sound button

(function () {
	const soundButton = $('#soundButton');
	const sound = $('#sound');

	sound[0].play();

	soundButton.click(function(e) {
		soundButton.toggleClass('is-muted');

		if (soundButton.hasClass('is-muted')) {
			sound[0].pause();
		} else {
			sound[0].play();
		}
	});

}());