// PopUp action

(function() {

	const btn = $('#popUpOpener');
	const popUp = $('#popUp');
	const popUpDarken = $('#popUpDarken');

	btn.click(function (e) {
		popUp.addClass('is-open');
		popUpDarken.addClass('is-open');
	});

	popUpDarken.click(function (e) {
		popUp.removeClass('is-open');
		popUpDarken.removeClass('is-open');
	});

}());