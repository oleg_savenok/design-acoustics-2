// Tariffs action

(function() {

	$('.tariffs_choice_item').hover(function (e) {
		const tariffIndex = e.target.dataset.tariff;

		const itemInfo = $('.tariffs_info_item');
		const allInfo = itemInfo;
		const targetInfo = allInfo[tariffIndex];

		const choice = $('.tariffs_choice');

		$(allInfo).removeClass('is-active');
		$(targetInfo).addClass('is-active');

		if (tariffIndex == 0) {
			choice.removeClass('is-comfort is-luxury');
			choice.addClass("is-free");
		} else
		if (tariffIndex == 1) {
			choice.removeClass('is-free is-luxury');
			choice.addClass('is-comfort');
		} else
		if (tariffIndex == 2) {
			choice.removeClass('is-free is-comfort');
			choice.addClass('is-luxury');
		}
	});

}());