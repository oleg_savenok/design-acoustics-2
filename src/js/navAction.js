// Nav actions

(function () {

	const nav = $('#nav');
	const hamburger = $('#hamburger');

	// Opening nav on click to hamburger
	hamburger.click(function(e) {
		nav.toggleClass('is-open');
	});

	// Closing nav on click to outside the element
	$('#slider').click(function(e) {
		if (nav.hasClass('is-open')) {
			nav.removeClass('is-open');
		}
	});

	// Closing nav on click to menu item or logo
	$('.nav_menu_item, .nav_logo').click(function(e) {
		nav.removeClass('is-open');
	});

}());